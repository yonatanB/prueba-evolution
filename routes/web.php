<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});


Route::resource('listado/tarea','TareaController'); //ruta principal asociada a el controlador.
Route::resource('seguridad/user','UserController'); //ruta principal asociada a el controlador.
Route::auth();

Route::get('/home','HomeController@index');
Route::get('/logout', 'Auth\LoginController@logout');