<?php

namespace evolution;

use Illuminate\Database\Eloquent\Model;

class Tarea extends Model
{
   
    protected $table='tareas';

    protected $primaryKey='idtarea';

    public $timestamps=false;


    protected $fillable =[ // campos que recibiran valores y se almacenaran en la base de datos

    	'nombre',
    	'prioridad',
    	'fecha'
    ];

    protected $guarded =[

    ];

}
