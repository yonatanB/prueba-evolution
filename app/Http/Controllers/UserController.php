<?php

namespace evolution\Http\Controllers;

use Illuminate\Http\Request;

use evolution\Http\Requests;
use evolution\User;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\input;
use Illuminate\Support\Facades\auth;
use evolution\Http\Requests\UserFormRequest;
use DB;

class UserController extends Controller
{
   public function __construct()
    {
          $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if ($request)
        {
            
            $users=DB::table('users')
            ->orderBy('name','desc')
            ->paginate(7);
            return view('seguridad.user.index',["users"=>$users]);// 
        }
    }
    public function create()
    {
        return view("seguridad.user.create"); // me retorna a la vista create donde se encuentra el formulario de ingreso de tareas
    }
    public function store (UserFormRequest $request)
    {
        
        $id = Auth::id();  //obtengo el id del usuario que se logea para solo mostrar las tareas de ese usuario.
        $user=new user; // creo una instancia del modelo tarea
        $user->name=$request->get('name');
        $user->email=$request->get('email');
        $user->password=bcrypt($request->get('password'));
        $user->save(); // llamo al metodo save para guardar los datos en la tabla tareas.
        return Redirect::to('seguridad/user'); // luego de enviar el formulario con el metodo post me redirecciona a la vista principal

    }



}
