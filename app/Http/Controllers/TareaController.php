<?php

namespace evolution\Http\Controllers;

use Illuminate\Http\Request;

use evolution\Http\Requests; // usar requerimientos http
use evolution\Tarea;//usar el modelo tarea
use Illuminate\Support\Facades\Redirect;// usar redirecciones
use Illuminate\Support\Facades\input;
use Illuminate\Support\Facades\auth;
use evolution\Http\Requests\TareaFormRequest; // archivo request con las reglas del formulario
use Laracasts\Flash\Flash;
use DB;

class TareaController extends Controller
{
     public function __construct()
    {
          $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if ($request)
        {
            $id = Auth::id();  //obtengo el id del usuario que se logea para solo mostrar las tareas de ese usuario.
            $tareas=DB::table('tareas')->where('id','=',$id)// alamaceno en la varible tareas la consulta a la tabla tareas del usuario id.
            ->orderBy('nombre','desc')
            ->paginate(7);

          $date = date('d-m-Y');

          foreach ($tareas as $tareavencer) {
              
              $date1=$tareavencer->fecha;

              $dias=(strtotime($date)-strtotime($date1))/86400; 
              $dias=abs($dias);  
              $dias=floor($dias); 

              if($dias<=3)
              {
                $tareav=$tareavencer->nombre;
                flash('Message')->warning("la tarea ".$tareav." esta proxima a vencer");
              }

          }
          
            return view('listado.tarea.index',["tareas"=>$tareas]);// 
        }
    }
    public function create()
    {
        return view("listado.tarea.create"); // me retorna a la vista create donde se encuentra el formulario de ingreso de tareas
    }
    public function store (TareaFormRequest $request)
    {
        
        $id = Auth::id();  //obtengo el id del usuario que se logea para solo mostrar las tareas de ese usuario.
        $tarea=new tarea; // creo una instancia del modelo tarea
        $tarea->id=$id;
        $tarea->nombre=$request->get('nombre');
        $tarea->prioridad=$request->get('prioridad');
        $tarea->fecha=$request->get('fecha');
        $tarea->save(); // llamo al metodo save para guardar los datos en la tabla tareas.
        return Redirect::to('listado/tarea'); // luego de enviar el formulario con el metodo post me redirecciona a la vista principal

    }
    public function edit($id)
    {
        return view("listado.tarea.edit",["tarea"=>tarea::findOrFail($id)]); // funcion que me redirecciona a la vista edit y envia el resgistro que se va a editar con su respectivo id.
    }
    public function update(TareaFormRequest $request,$id)
    {
        $tarea=tarea::findOrFail($id); // busco el resgistro con el id recibido
        $tarea->nombre=$request->get('nombre');
        $tarea->prioridad=$request->get('prioridad');
        $tarea->fecha=$request->get('fecha');
        $tarea->update(); // edito el resgistro e ingreso los datos.
        return Redirect::to('listado/tarea'); // me redirecciona a la vista principal
    }
    public function destroy($id)
    {
        $tarea=tarea::findOrFail($id);
        $tarea->delete(); // se elimina la tarea de la tabla tareas. por cuestiones de rapidez se utiliza este metodo, pero lo mas conveniente seria modificar un dato de la tabla que me va a representar la condicion de la tarea como 1- mostrar y 0-no mostrar(eliminar).
        return Redirect::to('listado/tarea');
    }

}
