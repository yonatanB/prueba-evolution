@extends ('layouts.admin')
@section ('contenido')
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h3>Editar Tarea: {{ $tarea->nombre}}</h3>
			@if (count($errors)>0)
			<div class="alert alert-danger">
				<ul>
				@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
			@endif

			{!!Form::model($tarea,['method'=>'PATCH','route'=>['tarea.update',$tarea->idtarea]])!!}
            {{Form::token()}}
            <div class="form-group">
            	<label for="nombre">Nombre</label>
            	<input type="text" name="nombre" class="form-control" value="{{$tarea->nombre}}" placeholder="nombre...">
            </div>
            <div class="form-group">
            	<label for="prioridad">Prioridad</label>
            	<select name="prioridad" class="form-control" value="{{$tarea->prioridad}}" >
                    <option value="ALTA">Alta</option> 
                    <option value="BAJA">Baja</option>
                  </select>
            </div>
            <div class="form-group">
            	<label for="fecha">Fecha</label>
            	<input type="date" name="fecha" class="form-control" value="{{$tarea->fecha}}" placeholder="fecha...">
            </div>
            <div class="form-group">
            	<button class="btn btn-primary" type="submit">Guardar</button>
            	<button class="btn btn-danger" type="reset">Cancelar</button>
            </div>

			{!!Form::close()!!}		
            
		</div>
	</div>
@endsection