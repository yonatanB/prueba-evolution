@extends ('layouts.admin')
@section ('contenido')
<div class="row">
  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12"> <!---diseño responsivo utilizando la rejilla blade-->

    @include('flash::message')
    <h3>Listado de tareas <a href="tarea/create"><button class="btn btn-success">Nueva Tarea</button></a></h3>
  </div>
</div>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><!---diseño responsivo utilizando la rejilla blade-->
    <div class="table-responsive">
      <table class="table table-striped table-bordered table-condensed table-hover">
        <thead>
          <th>Nombre</th>
          <th>Prioridad</th>
          <th>Fecha</th>
        </thead>
               @foreach ($tareas as $tar)
        <tr>
          <td>{{ $tar->nombre}}</td>
          <td>{{ $tar->prioridad}}</td>
          <td>{{ $tar->fecha}}</td>
          <td>
            <a href="{{URL::action('TareaController@edit',$tar->idtarea)}}"><button class="btn btn-info">Editar</button></a>
                         <a href="" data-target="#modal-delete-{{$tar->idtarea}}" data-toggle="modal"><button class="btn btn-danger">Eliminar</button></a>
          </td>
        </tr>
        @include('listado.tarea.modal')
        @endforeach
      </table>
    </div>
    {{$tareas->render()}}
  </div>
</div>

@endsection