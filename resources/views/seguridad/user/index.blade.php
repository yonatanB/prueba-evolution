@extends ('layouts.admin')
@section ('contenido')
<<div class="row">
  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12"> <!---diseño responsivo utilizando la rejilla blade-->

      <h3>Listado de Usuarios <a href="user/create"><button class="btn btn-success">Nuevo Usuario</button></a></h3>
  </div>
</div>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><!---diseño responsivo utilizando la rejilla blade-->
    <div class="table-responsive">
      <table class="table table-striped table-bordered table-condensed table-hover">
        <thead>
          <th>Nombre</th>
          <th>Email</th>
          <th>Password</th>
        </thead>
               @foreach ($users as $us)
        <tr>
          <td>{{ $us->name}}</td>
          <td>{{ $us->email}}</td>
          <td>{{ $us->password}}</td>
        </tr>
        @endforeach
      </table>
    </div>
    {{$users->render()}}
  </div>
</div>

@endsection