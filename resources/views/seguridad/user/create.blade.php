@extends ('layouts.admin')
@section ('contenido')
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h3>Nueva Usuario</h3>
			@if (count($errors)>0)
			<div class="alert alert-danger">
				<ul>
				@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
			@endif

			{!!Form::open(array('url'=>'seguridad/user','method'=>'POST','autocomplete'=>'off'))!!}
            {{Form::token()}}
            <div class="form-group">
            	<label for="name">Nombre</label>
            	<input type="text" name="name" class="form-control" placeholder="Nombre...">
            </div>
            <div class="form-group">
                  <label for="name">Email</label>
            	<input type="email" name="email" class="form-control" placeholder="email...">           
            </div>
            <div class="form-group">
            	<label for="password">Password</label>
            	<input type="Password" name="password" class="form-control" placeholder="Contraseña...">
            </div>
            <div class="form-group">
                  <label for="confir"> Confirm Password</label>
                  <input type="Password" name="confir" class="form-control" placeholder="Confirmar Contraseña...">
            </div>
            <div class="form-group">
            	<button class="btn btn-primary" type="submit">Guardar</button>
            	<button class="btn btn-danger" type="reset">Cancelar</button>
            </div>

			{!!Form::close()!!}		
            
		</div>
	</div>
@endsection